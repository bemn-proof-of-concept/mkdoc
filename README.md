# mkdoc

Trying MkDocs: https://www.mkdocs.org

See the gitlab page: https://bemn-proof-of-concept.gitlab.io/mkdoc/

## up and run 

Assuming you already have **python** and **pip**

```bash
cd src/hkypl
pip install -r requirements.txt
```

```bash
cd src/hkypl
mkdocs serve
```