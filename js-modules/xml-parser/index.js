// Import a module using ES6 import syntax
import axios from 'axios'
import queryString from 'query-string'
import "@babel/polyfill"
import Parser from "rss-parser"
import Handlebars from 'handlebars'

// console.log(location.search);
// //=> '?foo=bar'
 

// // Want to use async/await? Add the `async` keyword to your outer function/method.
// async function getXml(channel_id) {
//     try {
//         const url = `https://www.youtube.com/feeds/videos.xml?channel_id=${ channel_id }`
//         console.debug(url);
//         const { data } = await axios({
//             url: "https://cors-anywhere.herokuapp.com/" + url,
//             method: 'GET',
//             crossdomain: true
//           })
//         // console.log(data);
//     } catch (error) {
//         console.error(error);
//     }
// }

const { channel_id } = queryString.parse(location.search);
console.debug(channel_id)
// getXml(channel_id);

// let Parser = require('rss-parser');
let parser = new Parser();

async function getRss (channel_id){
    // const channel_id = "UCoVycxbCXEsd-mrP83EqVWQ"
    const rss = `https://www.youtube.com/feeds/videos.xml?channel_id=${ channel_id }`
  let feed = await parser.parseURL("https://cors-anywhere.herokuapp.com/" + rss);
  console.log(feed);

  feed.items.forEach(item => {
    console.log(item.title + ':' + item.link)
  });

  return feed
}

// var source = "<p>Hello, my name is {{name}}. I am from {{hometown}}. I have " +
//              "{{kids.length}} kids:</p>" +
//              "<ul>{{#kids}}<li>{{name}} is {{age}}</li>{{/kids}}</ul>";
(async() => {
    // var source = "<ul>{{#each items}}<li><h1>{{title}}</h1><code>{{link}}</code></li>{{/each}}</ul>"

    var source = `<h1>{{title}}<div class="g-ytsubscribe" data-channelid="${ channel_id }" data-layout="full" data-theme="dark" data-count="default"></div></h1>` + 
    `<ul>{{#each items}}<li><h2><a href="{{link}}" target="_blank">{{title}}</a></h2><p><date>{{pubDate}}</date></p><iframe width="560" height="315" src="{{embed}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></li><hr />{{/each}}</ul>`

    var template = Handlebars.compile(source);
     
    var data = await getRss(channel_id);
    data.items.forEach(x => {
        let link = x.link;
        let vid = queryString.parseUrl(link).query.v
        
        x["embed"] = `https://www.youtube.com/embed/${ vid }`
    });
    const list = data.items.slice(0, 5)
    console.table(list.map(x => x.embed))
    data.items = list;
    var result = template(data);
    
    document.body.innerHTML += result;
})();