頻道名 | 類型#1 | 類型#2 | 類型#3 | 訂閱人數
---------- | ---------- | ---------- | ---------- | ---------:
蘋果動新聞 HK Apple Daily | 時事 | 賽馬 | 直播 | 1,630,000
JASON(大J) | 生活 | - | - | 950,000
RTHK 香港電台 | 電台 | 時事 | 文化 | 925,000
笑波子 | 電玩 | - | - | 894,000
點 Cook Guide | 飲食 | 烹飪 | - | 801,000
Mira's Garden | 生活 | 韓國 | - | 707,000
memehongkong | 時事 | 文化 | 經濟 | 621,000
杜汶澤喱騷 Chapman To’s Late Show | 娛樂 | 飲食 | 直播 | 582,000
FHProductionHK | 娛樂 | - | - | 581,000
煮家男人 Bob's Your Uncle | 飲食 | 直播 | 飛機 | 548,000
壹週刊 NEXT | 時事 | 清談 | - | 516,000
蛇仔明 | 生活 | 童年 | - | 466,000
GamePlayHK短片攻略 | 電玩 | - | - | 459,000
D100 Radio | 電台 | 時事 | 經濟 | 459,000
屎萊姆瘋狂世界 | 電玩 | 開箱 | - | 435,000
屎萊姆的3次元 | 電玩 | - | - | 407,000
Hana Tam | 生活 | 裝扮 | 音樂 | 402,000
海恩奶油 HeinCream | 生活 | 台灣 | - | 367,000
Carl Ho卡爾 頻道 | 生活 | 知識 | - | 349,000
Professor PowPow | 神秘 | 知識 | - | 346,000
飲食男女 | 飲食 | - | - | 344,000
arhoTV | 娛樂 | 生活 | - | 342,000
haywong709 | 飲食 | 烹飪 | - | 336,000
gingerlemoncola | 娛樂 | - | - | 332,000
KZee | 電玩 | - | - | 328,000
職人吹水 | 飲食 | - | - | 328,000
果籽 | 文化 | - | - | 327,000
lizzydaily | 裝扮 | 旅遊 | - | 322,000
RickyKAZAF | 裝扮 | - | - | 315,000
owoかや | 舞蹈 | 文化 | - | 302,000
商業電台 Hong Kong Toolbar | 電台 | - | - | 274,000
Ding Ding | 生活 | 裝扮 | - | 268,000
MIHK.tv_Youtube第一台 | 時事 | 清談 | - | 267,000
Ahhin (Hins) | 電玩 | - | - | 260,000
香江望神州 | 時事 | - | - | 255,000
852郵報 | 時事 | - | - | 255,000
我要做富翁 | 經濟 | - | - | 255,000
有線電視 CABLE TV & 有線新聞 CABLE News | 時事 | - | - | 255,000
Alfred Chan | 飲食 | - | - | 252,000
mandies kwok 肥蛙 | 旅遊 | 美術 | - | 250,000
城寨 Singjai | 電台 | 時事 | - | 245,000
Loui5Ng | 電玩 | - | - | 244,000
MIHK VLOG | 娛樂 | 挑戰 | - | 242,000
Ha Alice | 生活 | 裝扮 | - | 232,000
波仔 Boris | 生活 | 姊弟 | - | 230,000
煮吧!煮飯仔 | 玩具 | - | - | 228,000
MyRadio Hong Kong | 直播 | 時事 | - | 228,000
毛記電視 | 娛樂 | 訪問 | - | 228,000
rickolam1 | 娛樂 | 娛樂 | - | 223,000
Ng Sam | 時事 | - | - | 220,000
umoviegroup | 生活 | 音響 | - | 208,000
Arm Channel TV | 娛樂 | 直播 | - | 208,000
升旗易得道 | 時事 | - | - | 206,000
Games Effect | 娛樂 | - | - | 205,000
Pumpkin Jenn | 裝扮 | - | - | 201,000
暗網仔 2.0 | 神秘 | 知識 | - | 201,000
Pomato 小薯茄 | 娛樂 | - | - | 199,000
MARY姐 | 生活 | 裝扮 | - | 192,000
阿聲(singsing524) | 娛樂 | - | - | 191,000
香港花生 | 電台 | 時事 | 經濟 | 189,000
宅男俱樂部 | 娛樂 | 已解散 | - | 184,000
Siu Siu | 娛樂 | 電玩 | - | 175,000
JASON的遊戲頻道 HunggyGames | 電玩 | - | - | 174,000
Smilemiann | 音樂 | 娛樂 | - | 172,000
屯門畫室 Tuen Mun Studio | 美術 | 網店 | - | 172,000
odyleung | 生活 | 汽車 | - | 171,000
啤梨頻道BarryMa TV | 電台 | 直播 | - | 169,000
A H F A M A K E U P | 裝扮 | - | - | 168,000
周庭チャンネル | 生活 | - | - | 167,000
Katy Cheung | 裝扮 | 生活 | - | 166,000
Gavinchiu趙氏讀書生活 | 時事 | 經濟 | 哲學 | 166,000
Cynthia Ip | 生活 | 裝扮 | - | 159,000
唔熟唔食 Cook King Room | 飲食 | 烹飪 | - | 158,000
Kingsan | 電玩 | - | - | 155,000
Ordinary Gaming Studio平民遊戲工作室 | 電玩 | - | - | 155,000
Sharon Yung | 生活 | - | - | 152,000
Gundam G | 娛樂 | 動畫 | - | 151,000
不滅墨水 | 娛樂 | - | - | 149,000
馬米高 Michael MMG | 英語 | - | - | 147,000
ASHA ETC | 生活 | - | - | 145,000
港燦會館 | 時事 | - | - | 143,000
MagicMissHunny | 生活 | 魔術 | - | 142,000
港。實測Testhongkong | 娛樂 | 街訪 | - | 142,000
屎嫂程程 | 生活 | - | - | 141,000
SzeTingWong黃詩婷 | 音樂 | - | - | 140,000
CapTV HK | 娛樂 | - | - | 140,000
Rock哥 | 時事 | 揭穿詐騙 | - | 137,000
ANSON8955 | 娛樂 | 電玩 | - | 137,000
伍妞有伍仔 | 生活 | - | - | 137,000
AUMAN | 旅遊 | 攝影 | 手足 | 134,000
TheLittleBalu | 生活 | 挑戰 | - | 133,000
慕容MoYung | 電玩 | - | - | 129,000
志森與志豪 | 時事 | - | - | 128,000
高清本土台 | 時事 | 清談 | - | 127,000
高登音樂台 HKGolden Music | 音樂 | - | - | 126,000
falllab | 情侶 | 生活 | - | 124,000
undergroundDV 達哥 | 電玩 | - | - | 123,000
家姐 Agnes | 生活 | - | - | 123,000
Keith Sir | 音樂 | 知識 | - | 122,000
Coco哥 | 生活 | 經濟 | 親子 | 119,000
隨喜遇見 | 生活 | 藝員 | - | 118,000
《看中國》香港頻道 | 時事 | 美國 | - | 117,000
視點31 | 時事 | 清談 | - | 116,000
Stormtrooper白兵 | 時事 | 直播 | AV | 115,000
在下葉問 | 電玩 | - | - | 111,000
WHIZOO | 娛樂 | 挑戰 | - | 111,000
MaMa Fish | 生活 | 親子 | 裝扮 | 110,000
DidoPopcorn 秋本桐 | 電玩 | - | - | 109,000
鄺俊宇 | 時事 | 議員 | - | 109,000
Victor Ng Ming Tak | 時事 | 經濟 | - | 108,000
L A Z Y D A Z Z Y | 裝扮 | 生活 | - | 107,000
Messiah2048 | 時事 | 議會 | - | 106,000
正宗教練 | 體育 | 籃球 | - | 104,000
Happy Amy | 飲食 | 烹飪 | - | 104,000
Happy Kongner | 文化 | 直播 | 電玩 | 103,000
cheerS beauty | 裝扮 | 親子 | 生活 | 102,000
徐時論TsuisTalk | 時事 | - | - | 101,000
Edwin H. | 科技 | - | - | 99,600
Sallys Toy | 情趣 | - | - | 97,100
芷鹿泥馬 | 娛樂 | 直播 | - | 96,900
防腐辦公室 | 電玩 | - | - | 96,100
Goomusic | 音樂 | - | - | 95,500
W. BabyShadow | 生活 | 裝扮 | - | 93,200
攪屎棍 PoopStirrer | 娛樂 | 音樂 | - | 93,200
挽歌之聲 | 音樂 | - | - | 92,700
貝遊日本 | 旅遊 | - | - | 92,100
TIK LEE | 電玩 | - | - | 91,000
Candies wawa | 生活 | 美術 | - | 90,900
悶透社gumboringer | 時事 | - | - | 90,300
黎特 KNIGHT | 動畫 | - | - | 89,400
ΣSigma二次元解密 | 電玩 | - | - | 88,900
有啖好食 Perfect Continuous Eat | 飲食 | - | - | 88,800
肥姨姨 fataunt | 生活 | 挑戰 | - | 87,500
HEYMAN LAM | 裝扮 | 旅遊 | - | 87,300
Coach Fui | 體育 | 籃球 | - | 86,200
膠街架Plasticguys | 娛樂 | - | - | 85,100
Bithia Lam | 生活 | 親子 | - | 85,000
Mic Mic Cooking | 飲食 | 烹飪 | - | 84,800
Cindy楊斯雅 | 情趣 | - | - | 84,800
Highway Recording | 神秘 | - | - | 84,600
李慧玲LIVE | 時事 | - | - | 83,800
FITDEVMO | 健身 | 旅遊 | - | 83,200
漏墨佬 | 娛樂 | 電玩 | - | 82,700
Kitling_ 雀斑妹 | 生活 | - | - | 82,400
feverSound com | 音響 | 科技 | - | 82,200
Baka Shiro小白 | 生活 | 電玩 | - | 82,100
HeyJenniFa | 裝扮 | 旅遊 | - | 82,100
希治閣遊戲情報科 | 電玩 | 直播 | - | 81,600
Paul Gor | 健身 | - | - | 80,200
Emily Lau | 生活 | - | - | 80,100
CrazyFamily | 娛樂 | 挑戰 | - | 80,000
成波之路 my ways to get fat | 飲食 | - | - | 79,800
kikolimakeup | 裝扮 | 親子 | - | 78,800
Two Bites | 飲食 | 烹飪 | - | 78,700
LifeLoser人生輸家 | 電玩 | - | - | 76,600
薩爾達達人 Zeldadaren | 電玩 | 直播 | - | 75,800
Benja & Momo Channel | 旅遊 | - | - | 74,500
KisaBBB | 生活 | 旅遊 | - | 74,300
ToNick | 音樂 | - | - | 73,800
堅離地球 · 沈旭暉 | 時事 | 國際 | - | 73,600
Dr. Rex 醫學幼稚園 | 知識 | 健康 | - | 73,300
大紀元新聞網 | 時事 | - | - | 73,200
Kiki & May - 隨意吃樂部 | 飲食 | 情侶 | - | 72,400
廢青男女 | 生活 | 情侶 | - | 71,300
Mario Hau 孝仔 | 旅遊 | - | - | 71,100
Artem Ansheles | 生活 | 俄羅斯 | - | 71,000
Jackz | 時事 | - | - | 71,000
TheWeirdoProduction | 娛樂 | - | - | 70,800
MiHK MOVIE | 娛樂 | - | - | 70,700
JFFT | 娛樂 | 挑戰 | - | 69,800
Kofgym | 健身 | - | - | 68,900
Creamy Yick | 生活 | - | - | 68,400
CatGirl貓女孩 | 生活 | 飲食 | - | 68,000
Comma | 時事 | 文化 | - | 66,900
Mingsze | 生活 | 親子 | - | 66,800
SAVI魔人 | 電玩 | - | - | 66,700
XRX | 汽車 | - | - | 66,200
男人EEETV | 娛樂 | - | - | 66,000
大阪ANDY哥 | 旅遊 | 情侶 | - | 65,700
MIHK.tv_Youtube第二台 | 神秘 | - | - | 65,600
potatofish yu | 生活 | 澳洲 | - | 64,500
TMHK - Truth Media (Hong Kong) | 時事 | 直播 | - | 63,600
WillWill FunHouse | 電玩 | - | - | 63,300
giveZAINamin | 文化 | - | - | 63,100
GF vs GF | 生活 | 情侶 | 性小眾 | 62,900
Poor travel x 2 Girls | 飲食 | 旅遊 | - | 62,500
RAYVEL Studios | 生活 | - | - | 62,500
MASTER ALLEY 達人巷 | DIY | - | - | 62,200
無料案內所 | 娛樂 | 日本 | - | 61,500
光頭幫 TomFatKi | 娛樂 | 戶外 | - | 60,500
塔哥 | 電玩 | - | - | 60,200
ISSAC YIU | 裝扮 | - | - | 59,100
衣架 Yika | 旅遊 | - | - | 58,400
理性分析Rational Football Analysis | 體育 | 足球 | - | 58,100
Patrick C | 電玩 | - | - | 58,000
Better Trading | 經濟 | - | - | 57,900
Cupid Producer丘品創作 | 時事 | 直播 | 神秘 | 57,200
billy2046 | 生活 | - | - | 56,100
J F Ü N G | 音樂 | - | - | 54,500
Stephen Leung 吃喝玩樂 | 飲食 | 旅遊 | - | 54,300
fio Chiu | 裝扮 | - | - | 54,000
Chill Up | 娛樂 | 街訪 | - | 53,500
Smallb | 生活 | - | - | 53,300
火都泥 | 生活 | - | - | 53,300
咖喱張 Curry Cheung | 生活 | 美術 | - | 53,200
ThisDV at YNaught | 娛樂 | - | - | 52,400
GREYTONE | 音樂 | - | - | 52,000
晴天林 SunnyLam | 音樂 | - | - | 51,800
雅軒Kinki KB Lam | 飲食 | - | - | 51,700
Stand News 立場新聞 | 時事 | - | - | 51,600
細佬Jonas | 生活 | - | - | 51,300
蛋糕星球 Cake's Planet | 攝影 | - | - | 51,000
Drum Pat. | 音樂 | 鼓手 | - | 51,000
Freeminder Emi | 生活 | 旅遊 | - | 50,800
ValorGears | 科技 | - | - | 50,800
Sai Ho Chan | 抗爭 | - | - | 50,400
與芬尼學英語 Finnie's Language Arts | 英語 | - | - | 50,300
Twinkle Tarot 星之塔羅占卜 | 神秘 | - | - | 50,200
黃偉民易經講堂 | 時事 | - | - | 49,800
adornAddiction | 生活 | DIY | - | 49,100
佛都有伙 Buddha Buddy | 清談 | 宗教 | - | 48,700
AkinaCHOI | 生活 | - | - | 48,600
Dora 多啦 | 生活 | - | - | 48,300
William Lo | 健身 | - | - | 48,200
Chilli Lucas - 智利仔 | 抗爭 | - | - | 47,900
Cup 媒體 Cup Media | 文化 | - | - | 47,800
Channel Giggs | 時事 | - | - | 47,700
影像補完計畫 | 時事 | 抗爭 | - | 47,400
珍言真語 // 大紀元 | 時事 | - | - | 47,400
Holy Force | 音樂 | - | - | 46,400
Cherry 小美 | 生活 | - | - | 46,300
SAMANTHA LUI | 裝扮 | 旅遊 | - | 46,000
Gordon Lau | 生活 | - | - | 45,800
EG大個仔 | 生活 | - | - | 45,100
Gary Kwan | 文化 | 清談 | - | 44,100
StandwithHK | 抗爭 | - | - | 43,700
Lok Cheung HK | 攝影 | - | - | 42,900
Rose Ma | 電玩 | - | - | 42,800
eetvhk | 時事 | - | - | 42,100
Wingto林泳淘 | 娛樂 | - | - | 42,100
個人意見獨立思考 | 時事 | - | - | 41,700
拉哥 Lai UP | 旅遊 | 文化 | - | 41,000
香港眾志 Demosisto | 時事 | - | - | 40,800
港仔愛台灣Neil in Taiwan | 生活 | 台灣 | - | 40,600
nickko ho | DIY | 玩具 | - | 40,600
高手紹 | 時事 | - | - | 40,200
YauMan游敏 | 電玩 | 生活 | - | 39,700
網上學習平台Beginneros | 知識 | - | - | 39,700
裝修電視 DECO-TV | DIY | 裝修 | - | 38,600
零製作 | 電玩 | - | - | 38,400
ANGELAMIUZ | 裝扮 | 旅遊 | - | 38,200
住加女人 CanWomen | 生活 | - | - | 37,700
Mechan 86 | 生活 | - | - | 36,800
住加男人CanMen | 生活 | - | - | 36,500
MVM | 時事 | - | - | 36,400
Raymond Regulus | 清談 | 時事 | - | 36,300
PassionTimes hk | 時事 | 電台 | - | 36,100
半職人妻 Halfwife | 旅遊 | - | - | 35,700
香城教育電視 HKGETV | 抗爭 | - | - | 35,200
HIDY YEUNG | 生活 | - | - | 34,900
imisshotmail | 音樂 | - | - | 34,900
TheBakerieHK | 音樂 | - | - | 34,700
Coco Cherry | 生活 | - | - | 34,400
A Dummie | 動畫 | SFM | - | 34,100
People Power | 時事 | 快必 | - | 33,600
乳透社·小反旗 | 音樂 | - | - | 33,400
Nova_AaronHK | 電玩 | 直播 | - | 33,200
五分鐘變大師 | 知識 | 生活 | - | 33,000
金剛Crew | 娛樂 | - | - | 33,000
Charmaine Fong | 音樂 | - | - | 32,800
Mike Yuen 袁竣鋒 | 旅遊 | - | - | 32,800
楊岳橋Alvin Yeung | 時事 | 議員 | - | 32,700
Flaviawg | 旅遊 | - | - | 32,500
LKNim | 科技 | - | - | 32,200
燒車Tielor'sPath | 生活 | - | - | 31,400
諧星組StarPlayersChannel | 娛樂 | 生活 | - | 31,300
IT9GameLog | 科技 | - | - | 31,300
Gutgut吉吉 | 裝扮 | - | - | 31,300
魚波 Yu Ball | 電單車 | - | - | 31,200
麻甩重工 | 電玩 | 玩具 | - | 31,000
玖零重工 | 電玩 | - | - | 30,900
殘楓CanFeng | 電玩 | - | - | 30,800
搣時潘Miss Pun | 生活 | - | - | 30,100
StorytellerHK | 文化 | 哲學 | - | 29,600
雞骨草 | 時事 | - | - | 29,300
Klafmann_HK_TW | 音樂 | - | - | 28,500
多事男人ToastMen | 時事 | 藝員 | - | 28,500
何Wayne足球評論 | 體育 | 足球 | - | 28,300
長腿木木 | 電玩 | - | - | 28,200
Tony Yim | 電玩 | - | - | 27,900
Siu Mei | 裝扮 | - | - | 27,900
周籽言Chosze | 音樂 | 貓奴 | - | 27,700
阿零的攝影日常 | 攝影 | - | - | 27,700
Sam先生 - 電影日誌 | 影視 | - | - | 27,700
SIU TING MAK | 時事 | 經濟 | - | 27,500
陳兩儀Chan 22 | 電玩 | - | - | 27,500
Hiddie T | 裝扮 | - | - | 27,300
大肥看天下 | 時事 | - | - | 26,900
時空咖啡館 timecoolfree | 文化 | 清談 | - | 26,400
heyoliztic | 音樂 | - | - | 26,000
冬OT | 娛樂 | - | - | 25,900
膠攝現場 | 攝影 | - | - | 25,600
地評線 HorizonPen | 時事 | - | - | 25,600
Get-GO | 健身 | - | - | 25,500
Saya Cheung | 電玩 | - | - | 24,900
凍豆腐 | 生活 | - | - | 24,000
梁芷珊Canny Leung | 生活 | - | - | 23,800
咩哥 | 玩具 | - | - | 23,500
堅庭 | 時事 | - | - | 23,400
Dgx music | 音樂 | - | - | 23,200
Booom HK | 娛樂 | - | - | 22,800
J2 Production | 電玩 | - | - | 22,800
阿東RangerEast | 電玩 | - | - | 22,600
PLAY U | 飲食 | - | - | 22,400
officialKOLORvids | 音樂 | - | - | 22,300
Little Big Chat | 時事 | - | - | 22,300
映畫製作社SP | 攝影 | - | - | 22,200
超有病的小明 | 裝扮 | - | - | 21,700
Kit Chau | 裝扮 | - | - | 21,600
Dottie Hidee | 裝扮 | - | - | 21,600
爆箱兄弟 | 玩具 | 開箱 | - | 21,600
依糕Eagle | 生活 | 台灣 | - | 21,500
JapHK LIVE | 電玩 | - | - | 21,300
藍天丨BlueSky | 電玩 | 直播 | - | 21,200
培仔Jacky | 旅遊 | - | - | 21,000
我睇咗啲乜嘢 | 娛樂 | - | - | 20,700
easonsiu | 音樂 | - | - | 20,600
Gordon Poon | 時事 | - | - | 20,600
JoyeeProduction | 戶外 | 旅遊 | 香港 | 20,500
蝦條哥 | 娛樂 | - | - | 19,800
Kyle Yeung | 音樂 | - | - | 19,700
BillyGoOut 比利出走活動組 | 戶外 | - | - | 19,400
Ms. Charlotte | 英語 | - | - | 19,100
MUSE CHAN | 攝影 | - | - | 19,100
國情揭露・中港台 | 時事 | - | - | 19,000
MiNAS 米納屍 | 音樂 | - | - | 18,700
Sony Chan | 知識 | 法文 | 生活 | 18,600
黄標軍師 奸人堅Water | 抗爭 | 生活 | - | 18,300
AP人生 | 戶外 | 直播 | 神秘 | 18,300
JFFLive | 直播 | 電玩 | 音樂 | 18,200
Tszzy | 電玩 | - | - | 18,100
小悠Yuu | 電玩 | 直播 | - | 17,500
鹿兒deer | 旅遊 | 電玩 | - | 17,400
Yellowland HK走訪深黃大地 | 抗爭 | 飲食 | - | 17,300
Ray Ho | 生活 | - | - | 17,200
Serrini | 音樂 | - | - | 17,200
EVITAvsEVITA | 裝扮 | 神秘 | - | 16,600
BusBoy巴士司機 | 生活 | - | - | 16,500
暴力 Violencee 生活誌 | 攝影 | 旅遊 | - | 16,500
Sona Tina | 生活 | 裝扮 | - | 16,300
Marc Yam | 知識 | 物理 | - | 16,200
Jer仔 | 生活 | 時事 | - | 16,200
Claudia Koh | 音樂 | - | - | 16,100
CherryVDO | 生活 | 大學 | - | 16,000
English Lesson For Free(dom) | 英語 | 直播 | - | 15,600
亦藍 INAN | 電玩 | 直播 | - | 15,400
Calvin歌唱小教室 | 音樂 | 教唱歌 | - | 15,400
能能糯米 RunRunRomeo | 電玩 | - | - | 15,100
emiliawyk | 生活 | 抗爭 | 情趣 | 15,100
觸執毛Chochukmo | 音樂 | - | - | 14,900
Matan Even | 抗爭 | - | - | 14,900
Gulu Ball | 電玩 | - | - | 14,700
Devils | 電玩 | - | - | 14,600
Passion Music Ministry | 音樂 | - | - | 14,400
SwearDay晴爆 | 電台 | 時事 | - | 14,400
中年大叔粒Sir | 音樂 | 結他 | - | 14,200
Ah LAI Channel | 攝影 | - | - | 14,200
martin oei | 時事 | - | - | 14,000
東大嶼 East Lantau | 動畫 | - | - | 13,700
Ash Choi | 文化 | - | - | 13,600
文宣巴MSB | 抗爭 | - | - | 13,500
KillerSoap殺手鐧 | 音樂 | - | - | 13,100
LIVEpost 拉播新聞 | 時事 | - | - | 13,000
LittleEggplanet 茄雲遊攝世界 | 攝影 | 旅遊 | - | 12,900
數碼捕籠 | 科技 | - | - | 12,800
ALCUBE | 抗爭 | 文化 | - | 12,700
Boy's Planet | 電玩 | - | - | 12,400
JG野人 | 體育 | 滑板 | - | 12,400
柔柔已上線 | 娛樂 | 生活 | - | 12,300
StageTube | 時事 | - | - | 12,000
Offer 哥 DIY Workshop | DIY | - | - | 11,900
地盤佬江湖 - 五金街 | DIY | 五金 | - | 11,600
FF教室 | 知識 | DSE | 生活 | 11,500
ItsDora | 裝扮 | - | - | 11,400
Ragazine | 時事 | 經濟 | - | 11,400
Clarence Lee | 旅遊 | - | - | 11,300
Himte | 電玩 | - | - | 10,900
howtindog | 直播 | 宗教 | 清談 | 10,800
張惠雅 Regen Cheung Official Channel | 生活 | 音樂 | - | 10,700
Ying Chi | 音樂 | - | - | 10,600
Shy | 電玩 | - | - | 10,600
立法會議員陳志全 | 時事 | 議員 | - | 10,400
夏蝦 | 娛樂 | - | - | 10,100
Anthony黃獎 | 影視 | 動畫 | - | 9,970
Apollo Channel | 抗爭 | 英國 | - | 9,960
foursonproduction | 娛樂 | - | - | 9,840
風琴奇俠卡洛斯 - Carlos the OrganMan | 科技 | 知識 | 生活 | 9,810
HEBEFACE | 電玩 | - | - | 9,750
尼亞の居酒屋 | 電玩 | - | - | 9,700
阿湯Tom | 旅遊 | - | - | 9,600
Kelvina Chung | 神秘 | - | - | 9,600
Y so Cerious | 生活 | 情侶 | - | 9,570
21研究室 | 知識 | 神秘 | - | 9,360
LittleNerve小神經 | 音樂 | - | - | 9,300
蛇仔 SnaKe | 生活 | 電玩 | - | 9,290
阿李 | 音樂 | - | - | 9,160
羅雲李 Lauren Lee Makeup | 裝扮 | 生活 | - | 9,140
窮遊達人 Mr.Travel Genius | 旅遊 | - | - | 9,110
Lau Kin Lam - 林仔 | 科技 | - | - | 8,990
Henry lam | 玩具 | - | - | 8,890
GDJYB雞蛋蒸肉餅 | 音樂 | - | - | 8,840
懵盛盛Sillysingsing | 音樂 | - | - | 8,830
中醫曼筆 | 體育 | 足球 | - | 8,720
塔妹 | 寵物 | 貓奴 | - | 8,680
Lil Ashes 小塵埃 | 音樂 | - | - | 8,330
OfficeDailyHK | 旅行 | - | - | 8,200
MemoSonBB | 娛樂 | - | - | 8,120
Brendan 毛爸 | 旅遊 | 寵物 | 狗主 | 7,960
Dion Tse | 生活 | - | - | 7,840
Willy Lee | 旅遊 | - | - | 7,810
19玩具頻道 | 玩具 | - | - | 7,770
COOL GAMING HK | 電玩 | - | - | 7,680
日向會社 shonentothesun | 經濟 | - | - | 7,600
Black Head Down - Hong Kong protest | 抗爭 | 國際 | - | 7,580
失更達人 | 清談 | - | - | 7,450
Angus安格斯教室 | 兩性 | 旅遊 | - | 7,180
Miles賣噢 | 動畫 | 娛樂 | - | 7,060
Kit Siu | 電玩 | - | - | 7,050
Neo Chan | DIY | 動畫 | - | 6,940
I am 歌莉亞 | 電台 | - | - | 6,850
Luna Is A Bep | 音樂 | - | - | 6,840
乜PAGE | 娛樂 | - | - | 6,750
CLS Express | 音樂 | - | - | 6,700
柳俊江Lauyeah | 體育 | 跑步 | 溝通 | 6,640
動漫廢物電台 | 電台 | 清談 | 動畫 | 6,620
放棄治療院 | 時事 | - | - | 6,600
迷途小書僮 KC | 文化 | - | - | 6,590
今天只想食玩訓 JoeKi | 飲食 | 旅遊 | - | 6,550
Lester莊正 | 音樂 | 手足 | - | 6,550
DJ NINJA | 電玩 | - | - | 6,460
好青年荼毒室 - 哲學部 | 哲學 | - | - | 6,450
煲劇廢噏Literal Nothing | 影視 | - | - | 6,360
Vivi Cheung | 生活 | 親子 | - | 6,280
咪摸Painting | 美術 | - | - | 5,960
tfvsjs | 音樂 | - | - | 5,950
Yoyokellee | 旅遊 | - | - | 5,900
zaki liu VDO | 生活 | - | - | 5,840
Mmpu114_LS | 知識 | DSE | 通識 | 5,620
天才 Genius Daily | 玩具 | 夾公仔 | - | 5,560
HIN 鐵騎四圍去 | 電單車 | - | - | 5,480
混血肥仔 | 娛樂 | 飲食 | - | 5,350
onki | 美術 | - | - | 5,330
以宅論宅 | 電玩 | 直播 | 動畫 | 5,290
Instinct of Sight | 音樂 | - | - | 5,180
CRAZY FAN電瘋線 | 旅遊 | - | - | 5,080
OdyleungGaming | 電玩 | 直播 | - | 4,920
Tomato陳美濤 | 知識 | 文學 | - | 4,860
MIHK.tv鱷魚談 | 時事 | 清談 | - | 4,790
許智峯 Hui Chi Fung | 時事 | 議員 | - | 4,760
Dokkan TV | 電玩 | - | - | 4,740
黃冠斌。州長 | 生活 | - | - | 4,720
曼聯球迷 -栢大爸 | 體育 | 足球 | - | 4,690
陳裕匡 | 抗爭 | 直播 | - | 4,660
SIC Plays | 電玩 | 生活 | - | 4,620
HKGoldenMrA | 電玩 | - | - | 4,570
Dickson Chan | 旅遊 | 戶外 | - | 4,520
tieshulan | 音樂 | - | - | 4,480
ONETAKE 一鏡過 | 娛樂 | - | - | 4,420
shingshing 誠誠 | 生活 | 挑戰 | - | 4,420
Photo Time HK攝。時間 | 攝影 | 旅遊 | - | 4,380
Sportsyeah 2 | 體育 | - | - | 4,280
per se | 音樂 | - | - | 4,260
陳仔 Ah Chan-真心食說-吃喝玩樂 | 飲食 | 烹飪 | - | 4,210
仇栩欣 Jocelyn Chau | 時事 | 議員 | - | 4,080
Chris HKdiary | 單車 | 生活 | - | 4,070
Fai Bro | 時事 | 哲學 | - | 4,050
馬嘉均 Mason Ma Music | 音樂 | - | - | 4,000
抗爭仔 | 抗爭 | - | - | 3,960
fibibibi | 旅遊 | 生活 | - | 3,960
一心會長要挑戰 | 娛樂 | - | - | 3,940
FaTKiT | 電玩 | 抗爭 | - | 3,810
HongKong Buses | 巴士 | 汽車 | - | 3,770
hkcoupleride26 | 電單車 | 情侶 | - | 3,750
Liu Ni | 音樂 | - | - | 3,720
Koala TV | 旅遊 | - | - | 3,720
LowTechShow奴隸獸 | 夾公仔 | 玩具 | - | 3,620
Wallis Cho | 音樂 | - | - | 3,610
菲利TV | 電玩 | - | - | 3,610
Stefano Lodola | 抗爭 | 音樂 | - | 3,500
T仔悠閒生活頻道 | 電台 | - | - | 3,430
Yack Studio | 音樂 | - | - | 3,400
Retroll Hip Hop重操嘻哈 | 音樂 | - | - | 3,370
奇異消失點 | 神秘 | - | - | 3,320
deb3927 | 音樂 | - | - | 3,180
Eric仔 | 音樂 | - | - | 3,180
豆一粒 | 生活 | - | - | 3,180
HAYSON BB & ERIC SO | 生活 | - | - | 3,150
TAXIGO打機講政時 | 電玩 | 時事 | - | 3,130
美劇癮 | 影視 | - | - | 3,080
HK Mini4WD Channel | 玩具 | - | - | 3,030
抗爭者V | 抗爭 | - | - | 3,010
講剷痴看 GCCH | 影視 | - | - | 2,990
Herman靴文 | 生活 | - | - | 2,990
Shing7160 | 電玩 | - | - | 2,960
夠講軍師Water 劉文克記 | 時事 | - | - | 2,930
Bikkuri Ashi | 音樂 | - | - | 2,880
San Francisco HongKongers | 抗爭 | 美國 | - | 2,860
香港人 | 時事 | 旅遊 | - | 2,800
HK MicroNews | 電玩 | - | - | 2,770
Portman Production | 科技 | 生活 | 電玩 | 2,740
A.T | 兩性 | - | - | 2,720
DSE IB GCSE Resources | 知識 | DSE | - | 2,690
桌遊港 BG Port HK - Hong Kong Boardgame Channel | 玩具 | - | - | 2,680
lui1201 Official | 電玩 | FIFA | - | 2,660
Lvcatable Cat Chan | 音樂 | - | - | 2,650
JJHin | 抗爭 | - | - | 2,620
Lee Siegfried | 電玩 | 懷舊 | - | 2,600
Q仔香港戶外直播 HK IRL | 戶外 | 直播 | 電玩 | 2,560
Aaron ON AIR | 攝影 | - | - | 2,550
香港人頻道 HongKonger Channel | 抗爭 | 攝影 | - | 2,540
餓底wingwing | 飲食 | ASMR | - | 2,530
ChunGMC | 電玩 | - | - | 2,490
汽水巴 | 抗爭 | - | - | 2,490
JaBi TV | 生活 | 飲食 | - | 2,490
chancheukb | 旅遊 | - | - | 2,480
Chill Hea 生活 | 音樂 | - | - | 2,430
Jon Jon Jonathan | 生活 | 娛樂 | - | 2,390
9up Youtuber | 生活 | 娛樂 | - | 2,380
SOSAD TV | 旅遊 | - | - | 2,340
送中反 | 抗爭 | - | - | 2,340
行山郊野樂Howard | 戶外 | - | - | 2,330
Cheng Tsz Shun | 生活 | - | - | 2,290
不再沉默2台 | 時事 | 直播 | - | 2,260
鄭達鴻Tat Cheng | 時事 | 議員 | - | 2,250
暗黑地牢 | 神秘 | - | - | 2,220
PSHK media official | 時事 | 直播 | - | 2,180
新德莉莉 New Dellily | 生活 | - | - | 2,140
時事乳牛 | 音樂 | - | - | 2,130
光顧香港時代美食 | 抗爭 | 飲食 | - | 2,090
yueculture | 哲學 | - | - | 2,080
ManiacOfficial | 音樂 | - | - | 1,950
The Glocal 全球政經評論 | 抗爭 | 國際戰線 | - | 1,950
Reorder Church 網路平台 | 文化 | - | - | 1,920
sethma317 | 電玩 | FIFA | - | 1,880
K.P | 動畫 | - | - | 1,850
Panda Gaming Channel | 電玩 | - | - | 1,840
透視新加坡SeeThrough Singapore | 生活 | 新加坡 | - | 1,830
牙楓AFung | 抗爭 | - | - | 1,820
Mixodus Music Production | 音樂 | 知識 | - | 1,800
DeepInside HK | 音樂 | - | - | 1,770
95 kit | 電玩 | FIFA | - | 1,770
子瑩_ChiyingChannel | 電玩 | - | - | 1,770
TraTra TV / Travis | 旅遊 | - | - | 1,750
樂卓奇 | 音樂 | - | - | 1,720
Goodbye HK Hello UK | 抗爭 | 英語 | - | 1,710
Bushman Kitchen | 戶外 | 烹飪 | - | 1,710
KKNS HONGKONG | 電玩 | - | - | 1,680
wow and flutter | 音樂 | - | - | 1,670
邊疆兄弟852BinGang | 音樂 | - | - | 1,650
KEN GOR | 旅遊 | - | - | 1,640
Eggplant Bus | 巴士 | - | - | 1,630
Hidden Yellow | 抗爭 | 黃店 | - | 1,590
James workshop | DIY | - | - | 1,560
小麻うまる malojam | 生活 | - | - | 1,540
袁海文 Ramon Yuen | 時事 | 議員 | - | 1,530
番茄 Tomato | 電玩 | MINECRAFT | - | 1,510
Dri VinCi | 汽車 | - | - | 1,500
Der仔 | 生活 | - | - | 1,500
Happy8 HK | 娛樂 | - | - | 1,500
Anthony Hong | 巴士 | - | - | 1,490
迷Rr路人 | 電玩 | 直播 | - | 1,470
USHB 特區聯合交通網 | 旅遊 | 香港 | - | 1,460
文宣女 字幕哥 | 抗爭 | - | - | 1,460
鞦韆先生 | 戶外 | - | - | 1,440
Part-Time Board Friend 香港桌遊頻道 | 玩具 | 桌遊 | - | 1,440
SoWhat | 音樂 | - | - | 1,430
鈦迪熊Dickson | 抗爭 | 生活 | - | 1,410
漫・遊世界Travelling Mandy | 旅遊 | - | - | 1,400
Ellis Chung | 戶外 | - | - | 1,370
講鐵 | 汽車 | 巴士 | - | 1,360
IMAGINEIIRECORDS | 音樂 | - | - | 1,360
肥康 | 電玩 | - | - | 1,350
諗嘢 tHinK | 娛樂 | - | - | 1,350
Breadpoon包包 | 生活 | 泰文 | 娛樂 | 1,330
Andrew Yun 的奇蹟世代 | 電玩 | 體育 | 籃球 | 1,310
Kuroneko Kyoko 黑貓響子 | 知識 | 日文 | - | 1,300
符家浚Calvert Fu | 音樂 | - | - | 1,300
2CM 苦男與小助手 | 生活 | 情侶 | - | 1,270
Make It Last HK | 音樂 | - | - | 1,270
抗爭者M | 抗爭 | 電玩 | - | 1,240
豐彤FTV | 娛樂 | 情侶 | - | 1,190
Claudia Hon | 裝扮 | - | - | 1,150
香港日報Hong Kong Daily Times | 時事 | 直播 | - | 1,130
LO Kin Hei 羅健熙 | 時事 | 議員 | - | 1,130
Froggie Couple | 生活 | 情侶 | - | 1,130
明泰弟弟的閒情日誌 | 抗爭 | - | - | 1,110
JanLin Daily Life | 玩具 | Pokemon | - | 1,110
Charlie Lam Production地鼠工作室 | 汽車 | 巴士 | - | 1,080
Lonely Fatty 隻胖 | 飲食 | 飲食 | - | 1,080
1012阿hei製造中 | 娛樂 | - | - | 1,080
My叔 李文意 | 寵物 | - | - | 1,050
Clarisse Yeung | 時事 | 議員 | - | 1,040
SportsYeah 體嘢 | 體育 | - | - | 1,030
KENOIZ | 電玩 | 知識 | - | 1,010
starwarsstudio 200 | 文化 | 歷史 | - | 994
丐幫 Beggaritch | 飲食 | - | - | 988
Jmdog | 娛樂 | - | - | 986
Bruce Ace’s magic | 影視 | - | - | 972
wearethud | 音樂 | - | - | 952
Dev G Chi | 知識 | 程式 | - | 946
1000訂閱挑戰中－IT仔的頻道 | 知識 | 程式 | - | 940
香港Science寺_HKScienceTemple | 知識 | 科學 | - | 933
咪咪 生存觀察日記 | 生活 | - | - | 933
Ezekiel 意識橋 | 知識 | - | - | 924
choisaiho | 音樂 | - | - | 918
Brian's Garage | 玩具 | - | - | 899
The Sulis Club | 音樂 | - | - | 897
HK Yutkeker | 音樂 | 抗爭 | - | 878
Jacky Lim Channel | 時事 | - | - | 878
ModernChildrenHK | 音樂 | - | - | 848
Tweety Tang | 裝扮 | - | - | 836
卡苗遊玩中 | 電玩 | - | - | 835
九型人格Wilson | 知識 | 心理 | - | 823
一同 One Tone | 娛樂 | - | - | 810
Boring仔 HK | 電玩 | - | - | 782
Stranded Whale | 音樂 | - | - | 775
Leung Jenny | 時事 | 議員 | - | 749
查理小新地Charlie and Sandi | 電玩 | 生活 | 直播 | 744
The Lesser Evil: | 裝扮 | - | - | 743
SA9 LAI 沙一世界 | 生活 | 旅行 | - | 743
小凜little cool的交通頻道 | 汽車 | 巴士 | - | 743
Bella Poon | 裝扮 | - | - | 741
城仔研究所 | 抗爭 | 電玩 | - | 739
Joseph Wan Music | 音樂 | - | - | 736
香港解構: Hong Kong Explained | 時事 | 抗爭 | - | 718
VOGUISH PROJECT | 裝扮 | - | - | 705
Honeybee930 | 懷舊 | 抗爭 | - | 703
真司 | 電玩 | - | - | 694
VBV | 生活 | 短劇 | - | 692
Fire Extinguisher 滅火筒 | 抗爭 | - | - | 692
fanhunga | 音樂 | - | - | 662
Jenna Chord | 音樂 | - | - | 636
Newoner 神經員 | 娛樂 | 街訪 | - | 636
林卓廷 LAM CHEUK TING | 時事 | 議員 | - | 631
Made of Water | 抗爭 | - | - | 622
Passenger C41 | 攝影 | - | - | 615
香港本地音樂推廣員 | 音樂 | PLAYLIST | - | 613
秋紅Qiu Hong | 音樂 | - | - | 601
DIMGUYS | 生活 | 時事 | - | 598
I'm a hongkonger我係香港人 | 時事 | - | - | 591
The Umbrella Union | 抗爭 | - | - | 590
豆豉蒸肉餅 Ben & Suky | 生活 | 情侶 | - | 588
willhappy __ | 電玩 | FIFA | - | 555
H3ith | 電玩 | - | - | 547
We Are HKers | 抗爭 | - | - | 544
講DRONE聞 | 科技 | - | - | 543
KP7024's Production | 汽車 | 巴士 | - | 527
Tecky Academy | 知識 | 程式 | - | 524
Yona channel [HKVLiver] | 電玩 | VTuber | - | 523
區諾軒 Nok Hin Au | 時事 | 議員 | 動漫 | 522
Hong Kong Reggae Band - Sensi Lion (粵音幻聽雷鬼 - 狻猊) | 音樂 | - | - | 517
eric poon | 抗爭 | - | - | 514
Savonnez C. | 裝扮 | 生活 | 娛樂 | 513
Yoman KM7215 | 巴士 | - | - | 507
十五度 Fifteen Degree Celsius | 音樂 | - | - | 501
Rocky's Studio | 生活 | 交通 | - | 494
多多遊 Dor! Au Voyage! | 旅行 | 飲食 | - | 488
Aiken艾肯 | 生活 | - | - | 488
丹尼爾體育館Daniel Stadium | 電玩 | 體育 | 籃球 | 475
Momoleitau無無厘頭 | 娛樂 | - | - | 467
5成熟 | 娛樂 | - | - | 453
KETER | 抗爭 | - | - | 451
胡說樹屋 - Treehouse PKMN | 玩具 | Pokemon | - | 446
Zhi Choi | 旅遊 | 音樂 | - | 437
Coolingmoon | 電玩 | - | - | 425
阿寶BO CHAN | 飲食 | 娛樂 | 裝扮 | 422
Melody Guys Music Group | 音樂 | - | - | 419
PlaypokemonhkVGC | 玩具 | Pokemon | - | 403
Ka Ki Kwok | 時事 | 議員 | - | 400
Lcwing | 電玩 | 直播 | - | 395
Will的發明工坊 | DIY | - | - | 385
Pennny music | 音樂 | - | - | 368
SpeedStar | 時事 | - | - | 362
man man Lee | 生活 | 娛樂 | - | 355
Amy仔 | 生活 | - | - | 339
素心 手制生活百貨 | 裝扮 | DIY | - | 330
譚文豪Jeremy Tam | 時事 | 議員 | - | 323
Spark・撻著 | 時事 | 街訪 | - | 321
Curry Gor | 生活 | 開箱 | - | 318
港女科學家 | 知識 | 科學 | - | 313
毒TV | 電玩 | 直播 | - | 311
KKVN急行 | 汽車 | 巴士 | - | 310
AndyIsTypingVEVO | 音樂 | - | - | 309
WA 8593 B8L Workshop | 巴士 | - | - | 309
FLC | 電玩 | 高達 | - | 307
中學鴨Secondary Duck🇭🇰🦆 | 時事 | 抗爭 | - | 295
雞蛋仔Chicken Egg Son | 生活 | - | - | 289
暗瘡女孩 Happy | 裝扮 | - | - | 277
李芷君Ada Lee | 音樂 | - | - | 277
Double Donut | 知識 | 動畫 | 數學 | 274
Meow&13plus2 | 寵物 | - | - | 274
Easy Production | 旅遊 | 電玩 | - | 271
Lottery248 | 電玩 | 英語 | Minecraft | 270
網台1999Channel | 電台 | - | - | 264
李奧 | 生活 | 時事 | - | 253
陳鈺琳Tramy Chan | 時事 | 議員 | - | 248
Harrison哈仔 | 飲食 | - | - | 243
位面守護者 | 電玩 | - | - | 243
FinF TV | 生活 | - | - | 241
HongKong Small potato | 抗爭 | - | - | 229
話題撚 | 時事 | 生活 | 影視 | 227
dja | 音樂 | - | - | 225
iCompass HK | 文化 | - | - | 224
jumblelife | 生活 | - | - | 223
CKW Production Studio | 巴士 | - | - | 221
Potter Corner | 音樂 | 動畫 | - | 213
設計師馬達 | 美術 | - | - | 194
BEN BEN BEN TV | 時事 | - | - | 193
煲仔愛港豬 Kong Pig is Yummy | 清談 | - | - | 184
Christine Daily | 清談 | - | - | 180
Hungry Kenny | 旅遊 | - | - | 180
PUBG Mobile Ver.2 Sai Chan 2099 | 電玩 | - | - | 163
Dumpoop 屎 Channel | 生活 | 瑞典 | - | 147
maRK Quartet | 音樂 | - | - | 146
WH1471紅鑽工作室 | 汽車 | 巴士 | - | 138
四眼仔s | 生活 | - | - | 136
指路明燈 | 時事 | - | - | 128
皇家貼圖工坊 | 抗爭 | 美術 | - | 127
Sunnylch 李焯豪 | 音樂 | - | - | 120
暗殞GKZ | 電玩 | - | - | 112
Random History | 文化 | 歷史 | 知識 | 108
HamMer Vlog | 生活 | - | - | 106
SiteAccessMusic | 音樂 | - | - | 104
HK Justin | 旅遊 | - | - | 102
Boris Chui火龍果 | 音樂 | 旅遊 | - | 98
Pw paul wong | 音樂 | 時事 | - | 95
聰辯先生 | 電玩 | Pokemon | - | 86
Echo chan 回音醬 | 生活 | - | - | 82
康伯 | 時事 | 飲食 | - | 82
ho ming | 旅遊 | - | - | 81
s_hongokong_er_s | 抗爭 | - | - | 72
Melomelomelo | 舞蹈 | 文化 | - | 64
Lukas K | 生活 | - | - | 63
Elaine Cheng | 生活 | - | - | 63
UM4176 Production | 巴士 | 汽車 | - | 61
LIHKG 偽善桐生 | 體育 | 足球 | - | 60
ROOT | 時事 | 街訪 | - | 59
JKI54A1 | 電玩 | 生活 | - | 59
不是歌手 | 音樂 | - | - | 57
泡槍. | 抗爭 | - | - | 54
Sui-Hin Mak | 音樂 | - | - | 50
可樂加煙法力無邊 | 時事 | - | - | 50
X - Factor | 抗爭 | - | - | 49
淑芬Sofun | 清談 | - | - | 40
Voice Deliverer | 配音 | 娛樂 | 動畫 | 36
Interesting Cooking Unit | 飲食 | 旅行 | - | 22
HK Justin production | 娛樂 | 抗爭 | - | 19
DigiFunHK | 飲食 | - | - | 18
mss文宣絲 | 抗爭 | - | - | 17
家偉Gary | 音樂 | 生活 | - | 16
DC EG | 娛樂 | - | - | 14
Ryan Cheng | 電玩 | - | - | 13
Vc Best | 抗爭 | - | - | 10
Samuel Ho | 攝影 | - | - | 0
智將情侶 | 娛樂 | 情侶 | - | 0
尹兆堅ANDREW WAN | 時事 | 議員 | - | 0
香港紀錄片拓展計劃Hong Kong Documentary Initiative | 影視 | - | - | 0
VinasGaming | 電玩 | - | - | 0
Yin Yeung | 時事 | - | - | 0
the interzone collective | 音樂 | - | - | 0
raw track | 音樂 | - | - | 0
bensonleung | 音樂 | 汽車 | - | 0
HK GDotTV | 性小眾 | - | - | 0
RJ HK | 抗爭 | - | - | 0
娜美_Nami | 生活 | - | - | 0
9:01AM Tv | 生活 | - | - | 0
Caitlyn 俄羅斯羅素的紅 | 生活 | 俄羅斯 | - | 0
杜比書房TOBYBOOKBOOK | 文化 | 閱讀 | - | 0
那個尋找冰塊的遙遠的下午 | 文化 | 閱讀 | - | 0
音美 yammy | 音樂 | 生活 | - | 0
STSK Travel Blog | 旅遊 | - | - | 0
