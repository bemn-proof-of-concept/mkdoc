require 'rubygems'
require 'bundler/setup'
require 'nokogiri'

doc = Nokogiri::XML(File.open("page1.xml"))
doc.remove_namespaces!

row_count = doc.css('entry > cell[col=1]').size
col_count = doc.css('entry > cell[row=1]').size
cell_count = row_count * col_count

keyname = ['icon', 'name', 'tag1', 'tag2', 'tag3', 'size', 'desc']
dict = []
md = ''
(1..row_count).each do |row|
    obj = {}
    (2..(col_count-1)).each do |col| # skip 'desc', 'remarks'
        obj[keyname[col-1]] = doc.css("cell[row=#{row}][col=#{col}]").first.parent.css('content').inner_text

    end
    # puts obj
    dict.push(obj)
end

File.write("../hkypl/docs/catalog.md", '')
dict.each_with_index do |item, index|
    # buf = item.values.join(" | ")
    buf = "#{item.values[0]} | #{item.values[1]} | #{item.values[2]} | #{item.values[3]} | #{item.values[4]}"#" | #{item.values[4]}"
    buf += "\n"
   if (index == 0)
    buf += "---------- | ---------- | ---------- | ---------- | ---------:"# | ----------"
    # buf += + (item.values.map { |x| "----------" }).join(" | ")
    buf += "\n"
   end
   File.write("../hkypl/docs/catalog.md", buf, mode: 'a') #append
end

# keys = doc.css('entry > cell[row=1]').map{ |n| n.inner_text }
# puts keys
# puts row_count, col_count